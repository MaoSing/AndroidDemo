package usecase

interface WeatherUseCase {
    fun getWeather(callBack: WeatherCallBack,ApiType:String)
}

interface WeatherCallBack{
    fun onError(message: String)
    fun onSussed(message: String)
    fun onStart(message: String)
}