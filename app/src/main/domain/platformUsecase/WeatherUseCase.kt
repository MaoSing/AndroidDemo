package platformUsecase

import android.util.Log
import api.API_URL
import api.WeatherData
import api.WeatherDataService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import usecase.WeatherCallBack
import usecase.WeatherUseCase
import java.util.concurrent.TimeUnit

class WeatherUseCase: WeatherUseCase {
    companion object{
        private var mInstance:WeatherUseCase? = WeatherUseCase()
        val WeatherData:BehaviorSubject<List<WeatherData.Records.Locations>> = BehaviorSubject.create()
        val instance: WeatherUseCase
            get() {
                if(mInstance == null) {
                    mInstance = WeatherUseCase()
                }
                return mInstance!!
            }
        fun destroy(){
            mInstance = null
        }
    }
    override fun getWeather(callBack: WeatherCallBack, ApiType: String) {
        callBack.onStart("Loading...")
        CoroutineScope(Dispatchers.IO).launch {
            val response = _getWeather(ApiType) ?: run {
                callBack.onError("取得失敗")
                return@launch
            }
            callBack.onSussed("取得成功")
            WeatherData.onNext(response)
        }
    }
    private suspend fun _getWeather(ApiType: String):List<WeatherData.Records.Locations>?{
        val client = OkHttpClient.Builder()
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(10,TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client)
            .build()

        val service = retrofit.create(WeatherDataService::class.java)
        val result = service.getDataAsync(ApiType,API_URL.authorization,API_URL.elementName,API_URL.sort).await()
        val response = result.body()


        response?.records?.locations?.let {
            return it
        }
        return null
    }


}