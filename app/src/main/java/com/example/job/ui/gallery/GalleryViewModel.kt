package com.example.job.ui.gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import manager.DisposeBagManager
import platformUsecase.WeatherUseCase

class GalleryViewModel : ViewModel() {

    val bag: CompositeDisposable = DisposeBagManager.getBag()

    val title: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    init {
        subscribeRX()
    }

    private fun subscribeRX() {
        WeatherUseCase.WeatherData
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy (
                onNext = {
                    title.value = it!![0].locationsName +" 各地區未來三天降雨機率"
                }
            ).addTo(bag)
    }
    fun onDestroy(){
        this.bag.dispose()
    }
}