package com.example.job.ui.gallery

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import api.API_URL
import com.example.job.R
import com.example.job.databinding.FragmentGalleryBinding
import platformUsecase.WeatherUseCase
import usecase.WeatherCallBack

class GalleryFragment : Fragment(), WeatherCallBack {

    private lateinit var galleryViewModel: GalleryViewModel
    private var _binding: FragmentGalleryBinding? = null
    lateinit var progressDialog : ProgressDialog

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProvider(this).get(GalleryViewModel::class.java)

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        progressDialog = ProgressDialog(this.context)

        val textView: TextView = binding.textGallery
        galleryViewModel.title.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        WeatherUseCase.instance.getWeather(this, API_URL.Kaohsiung)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        galleryViewModel.onDestroy()
        _binding = null
    }
    override fun onError(message: String) {
        progressDialog.setMessage(message)
    }

    override fun onSussed(message: String) {
        progressDialog.setMessage(message)
        progressDialog.dismiss()
    }

    override fun onStart(message: String) {
        progressDialog.setMessage(message)
        progressDialog.show()
    }
}