package api

enum class LocationCode(var value: String) {


//    companion object {
//        private val values = values();
//        fun getByValue(value: String) = values.firstOrNull { it.value == value }
//    }
}


class API_URL {
    companion object {
        val baseUrl:String = "https://opendata.cwb.gov.tw"
        val authorization:String = "CWB-1ED879F9-5CAC-40BE-9B3B-E272F1EA7B9C"
        val elementName:String = "PoP6h"
        val sort:String="time"

        const val TaiChung = "F-D0047-073"
        const val Taipei = "F-D0047-061"
        const val Kaohsiung = "F-D0047-065"
    }
}