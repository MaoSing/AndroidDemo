package api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

data class WeatherData(
    val success: Boolean? = null,
    val result: Result? = null,
    val records: Records? = null
) {
    data class Result(
        val resource_id: String? = null,
        val fields: List<Fields>? = null
    ) {
        data class Fields(
            val id: String? = null,
            val type: String? = null
        )
    }

    data class Records(
        val locations: List<Locations>? = null,
    ) {
        data class Locations(
            val datasetDescription:String? = null,
            val locationsName:String? = null,
            val dataid:String? = null,
            val location:List<Location>? = null
        ) {
            data class Location(
                val locationName: String? = null,
                val geocode: String? = null,
                val lat:String? = null,
                val lon:String? = null,
                val weatherElement: List<WeatherElement>? = null
            ) {
                data class WeatherElement(
                    val elementName: String? = null,
                    val description: String? = null,
                    val time:List<Time>? = null
                ) {
                    data class Time(
                        val startTime: String? = null,
                        val endTime: String? = null,
                        val elementValue: List<ElementValue>? = null
                    ) {
                        data class ElementValue(
                            val value:String? = null,
                            val measures:String? = null
                        )
                    }
                }
            }
        }
    }
}
// ?Authorization=CWB-1ED879F9-5CAC-40BE-9B3B-E272F1EA7B9C
// &locationName=羅東鎮
// &elementName=Wx
interface WeatherDataService {

    @Headers(
        "Content-Type: application/json",
        "cache-control: no-cache",
    )
    @GET("api/v1/rest/datastore/{ApiType}")
    fun getDataAsync(@Path("ApiType") ApiType:String, @Query("Authorization") Authorization:String, @Query("elementName") elementName:String, @Query("sort") sort:String ): Deferred<Response<WeatherData>>
}