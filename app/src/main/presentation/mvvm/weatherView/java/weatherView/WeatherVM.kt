package weatherView

import ViewModelInterface
import api.WeatherData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import manager.DisposeBagManager
import platformUsecase.WeatherUseCase

class WeatherVM : ViewModelInterface {
    var weatherAdapter:BehaviorSubject<WeatherAdapter> = BehaviorSubject.create()

    override val bag: CompositeDisposable = DisposeBagManager.getBag()
    override fun onDestroy() {
        this.bag.dispose()
    }

    override fun subsribeRX() {

    }

}