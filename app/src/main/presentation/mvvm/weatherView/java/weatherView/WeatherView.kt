package weatherView

import ViewInterface
import ViewModelInterface
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import api.WeatherData
import io.reactivex.disposables.CompositeDisposable
import manager.DisposeBagManager
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry

import com.github.mikephil.charting.data.BarData


import com.github.mikephil.charting.charts.BarChart

import com.example.job.R
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.IAxisValueFormatter


class WeatherView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), ViewInterface {

    override val bag: CompositeDisposable = DisposeBagManager.getBag()
    override val viewModel: ViewModelInterface = WeatherVM()

    private var mBarChart: BarChart? = null
    private var mBarData: BarData? = null
    private var xValues: ArrayList<String>? = null
    private var mMap = mutableMapOf<String,String>()
    private var list:WeatherData.Records.Locations.Location? = null

    fun setData(location: WeatherData.Records.Locations.Location){
        mBarChart = findViewById(R.id.barChart)
        list = location

        addData()
        initData()
        setLegend()
        setXAxis()
        setMRAxis()
    }

    private fun addData(){
        val list = list!!.weatherElement!![0].time!!.toList()
        for (ele in list) {
            val startTime = ele.startTime
            val y = ele.elementValue!![0].value
            mMap[startTime.toString()] = y.toString()
        }
    }
    fun initData() {//初始化数据
        val yValues = java.util.ArrayList<BarEntry>()
        xValues = ArrayList()
        var count=0
        for (en in mMap){
            yValues.add(BarEntry(count.toFloat(), en.value.toFloat()))
            val label = en.key.split(" ")[1].replace(":00","")+":00"
            xValues!!.add(label)
            count+=1
        }
        val barDataSet = BarDataSet(yValues, "条形图")
        barDataSet.color = Color.parseColor("#99D9BE")//设置条形图的颜色值
        barDataSet.highLightColor = Color.rgb(39, 211, 138)//设置被选中的颜色值
        barDataSet.setDrawValues(false)//不设置条形图上的数字显示
        mBarData = BarData(barDataSet)
    }

    private fun setLegend() {//设置legend相关内容
        mBarChart!!.description.isEnabled = false//不设置描述
        mBarChart!!.data = mBarData
        mBarChart!!.setDrawMarkers(true)
        mBarChart!!.setScaleEnabled(false)
        mBarChart!!.legend.isEnabled = false//设置legend
    }

    private fun setXAxis(){//x轴相关设置
        val xAxis: XAxis = mBarChart!!.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.labelCount = xValues!!.count()
        xAxis.position = XAxis.XAxisPosition.BOTTOM // 位于底部

        xAxis.setDrawGridLines(false)
        xAxis.setDrawGridLines(false) // 不绘制X轴网格线
        xAxis.valueFormatter = IAxisValueFormatter { value, _ -> java.lang.String.valueOf(xValues!![value.toInt()]) }
    }

    private fun setMRAxis() {//y轴相关设置
        val mRAxis = mBarChart!!.axisRight
        mRAxis.isEnabled = false
        val mLAxis = mBarChart!!.axisLeft
        mLAxis.setDrawAxisLine(false)
        mLAxis.setDrawGridLines(false)
        mLAxis.axisMinimum = 0f
        mLAxis.axisMaximum = 100f
        mLAxis.labelCount = 5
        mBarChart!!.requestLayout()
    }
}