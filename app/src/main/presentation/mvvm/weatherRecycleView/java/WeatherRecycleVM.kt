package weatherView

import ViewModelInterface
import android.util.Log
import api.WeatherData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import manager.DisposeBagManager
import platformUsecase.WeatherUseCase

class WeatherRecycleVM : ViewModelInterface {
    var weatherAdapter:BehaviorSubject<WeatherAdapter> = BehaviorSubject.create()

    override val bag: CompositeDisposable = DisposeBagManager.getBag()
    override fun onDestroy() {
        this.bag.dispose()
    }

    override fun subsribeRX() {
        WeatherUseCase.WeatherData
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy (
                onNext = {
                    weatherAdapter.onNext(WeatherAdapter(it!![0].location!!))
                }
            ).addTo(bag)
    }
}