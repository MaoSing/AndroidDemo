package weatherView

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import api.WeatherData
import com.example.job.R
import com.github.mikephil.charting.charts.BarChart

class WeatherAdapter(private val weatherDataList: List<WeatherData.Records.Locations.Location>) :
    RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mBarChart: BarChart? = null
        var locationName: TextView? = null
        init {
            // Define click listener for the ViewHolder's View.
            mBarChart = view.findViewById(R.id.barChart)
            locationName = view.findViewById(R.id.locationName)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.weather_view, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        (viewHolder.itemView as WeatherView).setData(weatherDataList[position])
        viewHolder.locationName?.text = weatherDataList[position].locationName
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = weatherDataList.size

}
