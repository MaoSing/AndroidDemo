package weatherView

import ViewInterface
import ViewModelInterface
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import api.WeatherData
import io.reactivex.disposables.CompositeDisposable
import manager.DisposeBagManager
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry

import com.github.mikephil.charting.data.BarData


import com.github.mikephil.charting.charts.BarChart

import com.example.job.R
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class WeatherRecycleView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr), ViewInterface {

    override val bag: CompositeDisposable = DisposeBagManager.getBag()
    override val viewModel: WeatherRecycleVM = WeatherRecycleVM()

    init {
        viewModel.subsribeRX()

        viewModel.weatherAdapter
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    this.adapter = it
                    val mLayoutManager = LinearLayoutManager(context)
                    mLayoutManager.orientation = LinearLayoutManager.VERTICAL
                    this.layoutManager = mLayoutManager
                }
            ).addTo(bag)
    }
}