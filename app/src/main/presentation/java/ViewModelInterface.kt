import io.reactivex.disposables.CompositeDisposable

interface ViewModelInterface {
    val bag: CompositeDisposable
    fun onDestroy()
    fun subsribeRX()
}