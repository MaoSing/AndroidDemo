package manager

import io.reactivex.disposables.CompositeDisposable

object DisposeBagManager {
    private var bagList :ArrayList<CompositeDisposable> = ArrayList()

    fun getBag(): CompositeDisposable {
        val bag = CompositeDisposable()
        bagList.add(bag)
        return bag
    }
    fun clear(){
        gameBagClear()
    }

    private fun gameBagClear(){
        for (bag in bagList){
            bag.clear()
            bag.dispose()
        }
        bagList = ArrayList()
    }
}