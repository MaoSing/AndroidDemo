import io.reactivex.disposables.CompositeDisposable

interface ViewInterface {
    val bag: CompositeDisposable
    val viewModel: ViewModelInterface

    fun onDestroy() {}
}